## Projeto de automação de testes de API utilizando Java com Rest-Assured

Documentação de como rodar os testes do projeto.

Este projeto de automação de API foi construido com Junit, RestFull, Java é Maven.

#### Pré-Requisito:

* Ter o `java 8` configurado na máquina
   * Rode o comando `javac -version` para garantir.
   
#### Preprarar o ambiente:

* Importar o projeto maven

#### Execução do projeto:

* Existe quatro pastas de diferentes testes dentro da pasta ```test```

   * ```aceitacao```
   * ```contrato```
   * ```funcional```
   * ```healthcheck```
* Cada pasta serve para guardar um tipo de testes feito na api do projeto.

```IDE intellij:```
* Exec:
    * Para rodar os testes exemplos que estão dento das pastas, é so clicar com o botão direito do mouse em ```run``` ou com um click em cima do test + ```Ctrl + Shift + F9```.
***
```IDE Visual Code:```
* Exec:
    * execute o comando na raiz do projeto: ```mvn clean test``` ou com o botão direito do mouse em ```run```.
***

#### Organização do projeto:

```Arquitetura:```
* Pastas com descrição de armazenamento.

```
Organização do projeto.

* src/
    main/
        java/nomeProjeto
            * basetest      (Pasta de configuração de request e reponse)
            * datafactory   (Pasta para armazenar metodos de chamada de queries)
            * dto           (Pasta para armazenamento de dto)
            * service       (Pastas para armazenamento de chamada na api)
            * util          (Pasta para armazenar valores padrôes e etc)

        java/resource
            * ambiente      (Pasta para armazenar valores padrôes de ambiente)
* src/
    test/
        java/nomeProjeto
            * aceitacao     (Pasta que armazena testes de aceitação)
            * contrato      (Pasta que armazena testes de contrato da api)
            * funcional     (Pasta que armazena testes funcional da api)
            * healthcheck   (Pasta que armazena testes de verificação da api)

        java/resource
            * jsonschemas   (Pasta para armazenar json schemas para comparação no teste de contrato)
```

###### Autor: 
```Mailton Nascimento```















