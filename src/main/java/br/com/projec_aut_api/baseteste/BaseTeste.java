package br.com.projec_aut_api.baseteste;

import br.com.projec_aut_api.util.Constantes;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;

import static io.restassured.RestAssured.requestSpecification;
import static io.restassured.RestAssured.responseSpecification;

public class BaseTeste implements Constantes {

    public static Object TOKEN;

    public static RequestSpecification reqSpec;
    public static ResponseSpecification resSpec;

    @BeforeClass
    public static void setup(){
        System.out.println("Conexão ok");
        RestAssured.baseURI = APP_BASE_URL + APP_BASE_PATH;
        //RestAssured.port = APP_PORT;
        //RestAssured.basePath = APP_BASE_PATH;

        RequestSpecBuilder reqBuider = new RequestSpecBuilder();
        reqBuider.setContentType(APP_CONTENT_TYPE);
        reqBuider.log(LogDetail.ALL);
        reqSpec = reqBuider.build();

        ResponseSpecBuilder resBuider = new ResponseSpecBuilder();
        //resBuider.expectResponseTime(Matchers.lessThan(MAX_TIMEOUT));
        resBuider.log(LogDetail.ALL);
        resSpec = resBuider.build();

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

    }

}

