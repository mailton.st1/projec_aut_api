package br.com.projec_aut_api.service;

import br.com.projec_aut_api.baseteste.BaseTeste;
import br.com.projec_aut_api.dto.CepDTO;
import br.com.projec_aut_api.dto.CepErroDTO;

import static io.restassured.RestAssured.*;
import static org.apache.http.HttpStatus.SC_OK;

public class CepService extends BaseTeste {

    public CepDTO realizarConsultaComCep(String cep) {

        CepDTO registro = given().
                spec(reqSpec).
                baseUri(baseURI).
                when().
                get(cep +"/"+"json").
                then().
                statusCode(SC_OK).
                spec(resSpec).
                extract().as(CepDTO.class);

        return registro;
    }

    public CepErroDTO erroCepInvalido(String cep) {

        CepErroDTO registro = given().
                    spec(reqSpec).
                    baseUri(baseURI).
                when().
                    get(cep +"/"+"json").
                then().
                    statusCode(SC_OK).
                    spec(resSpec).
                    extract().as(CepErroDTO.class);

        return registro;

    }

    public CepDTO[] consultaComUfCidadeBairro(String uf, String cidade, String bairro){
        // fazendo um Post no path "/contas", e agora incluindo usuário
        CepDTO[] registro = given().
                    spec(reqSpec).
                    baseUri(baseURI).
                when().
                    get(uf+"/"+cidade+"/"+bairro+"/"+"json").
                then().
                    statusCode(SC_OK).
                    spec(resSpec).
                    extract().as(CepDTO[].class);

         return registro;
    }

    public CepErroDTO[] consultaComUfCidadeBairroPassandoDadoInvalido(String uf, String cidade, String bairro){

        // fazendo um Post no path "/contas", e agora incluindo usuário
        CepErroDTO[] registro = given().
                    spec(reqSpec).
                    baseUri(baseURI).
                when().
                    get(uf+"/"+cidade+"/"+bairro+"/"+"json").
                then().
                    statusCode(SC_OK).
                    spec(resSpec).
                    extract().as(CepErroDTO[].class);

        return registro;
    }

}