package br.com.projec_aut_api.service;

import br.com.projec_aut_api.baseteste.BaseTeste;
import br.com.projec_aut_api.dto.CepDTO;
import br.com.projec_aut_api.dto.CepErroDTO;
import br.com.projec_aut_api.dto.ListaCepDTO;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.hamcrest.Matchers;

import java.io.File;

import static io.restassured.RestAssured.*;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;

public class ContratoService extends BaseTeste {

    public void contratoCep(String cep) {
        given().
                given().
                    spec(reqSpec).
                    baseUri(baseURI).
                when().
                    get(cep +"/"+"json").
                then().
                    spec(resSpec).
                    statusCode(SC_OK).
                    assertThat().
                    body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonschemas/response-cep.json")));
    }
}