package br.com.projec_aut_api.aceitacao;

import br.com.projec_aut_api.baseteste.BaseTeste;
import br.com.projec_aut_api.dto.CepDTO;
import br.com.projec_aut_api.dto.CepErroDTO;
import br.com.projec_aut_api.service.CepService;

import org.testng.Assert;
import org.testng.annotations.Test;


public class CepAceitacaoTest extends BaseTeste {

    CepService cepService = new CepService();

    @Test(groups = {"aceitacao"})
    public void realizarConsultaComCepValido(){

        //Fazendo uma request do tipo GET passando o CEP na url
        CepDTO resultFromREST = cepService.realizarConsultaComCep("93110120");

        //Verificando o retorno da lista dentro do dto CepDTO
        Assert.assertEquals(resultFromREST.getCep(),"93110-120");
        Assert.assertEquals(resultFromREST.getLogradouro(),"Rua Leopoldo Freire Pinto");
        Assert.assertEquals(resultFromREST.getComplemento(),"");
        Assert.assertEquals(resultFromREST.getBairro(),"Rio dos Sinos");
        Assert.assertEquals(resultFromREST.getLocalidade(),"São Leopoldo");
        Assert.assertEquals(resultFromREST.getUf(),"RS");
        Assert.assertEquals(resultFromREST.getIbge(),"4318705");
        Assert.assertEquals(resultFromREST.getGia(),"");
        Assert.assertEquals(resultFromREST.getDdd(),"51");
        Assert.assertEquals(resultFromREST.getSiafi(),"8877");
    }

    @Test(groups = {"aceitacao"})
    public void realizarConsultaPassandoUfCidadeBairroValido(){

        CepDTO[] resultFromREST = cepService.consultaComUfCidadeBairro("RS","Gravatai","Barroso");

        //Verificando o retorno do array de listas na posição [0] dentro do dto CepDTO
        Assert.assertEquals(resultFromREST[0].getCep(),"94085-170");
        Assert.assertEquals(resultFromREST[0].getLogradouro(),"Rua Ari Barroso");
        Assert.assertEquals(resultFromREST[0].getComplemento(),"");
        Assert.assertEquals(resultFromREST[0].getBairro(),"Morada do Vale I");
        Assert.assertEquals(resultFromREST[0].getLocalidade(),"Gravataí");
        Assert.assertEquals(resultFromREST[0].getUf(),"RS");
        Assert.assertEquals(resultFromREST[0].getIbge(),"4309209");
        Assert.assertEquals(resultFromREST[0].getGia(),"");
        Assert.assertEquals(resultFromREST[0].getDdd(),"51");
        Assert.assertEquals(resultFromREST[0].getSiafi(),"8683");

        //Verificando o retorno do array de listas  na posição [1] dentro do dto CepDTO
        Assert.assertEquals(resultFromREST[1].getCep(),"94175-000");
        Assert.assertEquals(resultFromREST[1].getLogradouro(),"Rua Almirante Barroso");
        Assert.assertEquals(resultFromREST[1].getComplemento(),"");
        Assert.assertEquals(resultFromREST[1].getBairro(),"Recanto Corcunda");
        Assert.assertEquals(resultFromREST[1].getLocalidade(),"Gravataí");
        Assert.assertEquals(resultFromREST[1].getUf(),"RS");
        Assert.assertEquals(resultFromREST[1].getIbge(),"4309209");
        Assert.assertEquals(resultFromREST[1].getGia(),"");
        Assert.assertEquals(resultFromREST[1].getDdd(),"51");
        Assert.assertEquals(resultFromREST[1].getSiafi(),"8683");

    }

    @Test(groups = {"aceitacao"})
    public void realizarConsultaComCepInvalido(){

        //Fazendo uma request do tipo GET passando o CEP na url
        CepErroDTO erroFromREST = cepService.erroCepInvalido("93110320");

        //Verificando o retorno do erro que está armazendo dentro do dto CepErroDTO
        Assert.assertEquals(erroFromREST.getErro(),"true");
    }



}