package br.com.projec_aut_api.funcional;

import br.com.projec_aut_api.baseteste.BaseTeste;
import br.com.projec_aut_api.dto.CepErroDTO;
import br.com.projec_aut_api.service.CepService;
import org.testng.Assert;
import org.testng.annotations.Test;


public class CepFuncionalTest extends BaseTeste {

    CepService cepService = new CepService();

    @Test(groups = {"aceitacao"})
    public void realizarConsultaPassandoUfCidadeBairroValidoF(){

        // Passando uma UF diferente do cadastro da API
        CepErroDTO[] erroFromREST = cepService.consultaComUfCidadeBairroPassandoDadoInvalido("AM","Gravatai","Barroso");

        // Testes funcionais de api verificamos os tratamentos de erro, ex: passando um dado invalido e api retorna um mensagem
        Assert.assertEquals(erroFromREST[0].getErro()," ");



    }


}